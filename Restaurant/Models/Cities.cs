﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Restaurant.Models
{
    public partial class Cities
    {
        public Cities()
        {
            Restaurants = new HashSet<Restaurants>();
        }

        public int Id { get; set; }
        [DisplayName("City")]
        public string CityName { get; set; }
        [DisplayName("Country")]
        public string CountryName { get; set; }
        [DisplayName("Full Name")]
        public string FullName { get; set; }
        [DisplayName("State Code")]
        public string StateCode { get; set; }
        [DisplayName("State")]
        public string StateName { get; set; }

        public virtual ICollection<Restaurants> Restaurants { get; set; }
    }
}
