﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Restaurant.Models
{
    public partial class Reviews
    {
        public int Id { get; set; }
        public int? RestaurantId { get; set; }
        [DisplayName("Rating")]
        public int? Rating { get; set; }
        [DisplayName("Rating Text")]
        public string RatingText { get; set; }
        [DisplayName("Full Review")]
        public string ReviewText { get; set; }
        [DisplayName("Review Date")]
        public string ReviewTimeFriendly { get; set; }
        [DisplayName("Reviewer Name")]
        public string CustomerName { get; set; }
        [DisplayName("Restaurant")]
        public virtual Restaurants Restaurant { get; set; }
    }
}
