﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Restaurant.Models
{
    public partial class Restaurants
    {
       

        public int Id { get; set; }
        public int CityId { get; set; }
        public int CuisineId { get; set; }
        [DisplayName("Cuisines")]
        public string Cuisines { get; set; }
        [DisplayName("Currency")]
        public string Currency { get; set; }
        [DisplayName("Establishment")]
        public string Establishment { get; set; }
        [DisplayName("Delivery")]
        public int? HasDelivery { get; set; }
        [DisplayName("Take Out")]
        public int? HasTakeaway { get; set; }
        [DisplayName("Address")]
        public string Address { get; set; }
        [DisplayName("City")]
        public string City { get; set; }
        [DisplayName("State Code")]
        public string StateCode { get; set; }
        [DisplayName("Locality")]
        public string Locality { get; set; }
        [DisplayName("Locality Verbose")]
        public string LocalityVerbose { get; set; }
        [DisplayName("Zip Code")]
        public string ZipCode { get; set; }
        [DisplayName("Menu")]
        public string MenuUrl { get; set; }
        [DisplayName("Name")]
        public string Name { get; set; }
        [DisplayName("Number")]
        public string Telephone { get; set; }
        [DisplayName("Price Range")]
        public int? PriceRange { get; set; }
        [DisplayName("Timings")]
        public string Timings { get; set; }
        [DisplayName("URL")]
        public string Url { get; set; }
        [DisplayName("Total Rating")]
        public string AggregateRating { get; set; }
        [DisplayName("Rating Text")]
        public string RatingText { get; set; }

        public virtual Cities CityNavigation { get; set; }
        public virtual Cuisines Cuisine { get; set; }
        public virtual ICollection<Covid19> Covid19 { get; set; }
        public virtual ICollection<Reviews> Reviews { get; set; }
        public Restaurants()
        {
            Covid19 = new HashSet<Covid19>();
            Reviews = new HashSet<Reviews>();
        }
    }
}
