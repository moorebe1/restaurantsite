﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Restaurant.Models
{
    public partial class Cuisines
    {
        public Cuisines()
        {
            Restaurants = new HashSet<Restaurants>();
        }

        public int Id { get; set; }
        [DisplayName("Cuisine Type")]
        public string CuisineName { get; set; }

        public virtual ICollection<Restaurants> Restaurants { get; set; }
    }
}
